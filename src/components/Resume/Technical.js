import React from 'react';
import PropTypes from 'prop-types';

import Skill from './Technical/Skill';

const getRows = (skill) => skill.sort((a, b) => {
  let ret = 0;
  if (a.skill > b.skill) ret = -1;
  else if (a.skill < b.skill) ret = 1;
  return ret;
}).map((skill, idx) => (
  <Skill
    data={skill}
    key={skill.title}
    last={idx === skill.length - 1}
  />
));

const Technical = ({ data }) => (
  <div className="courses">
    <div className="link-to" id="Technical" />
    <div className="title">
      <h3>Technical Skills</h3>
    </div>
    <ul className="course-list">
      {getRows(data)}
    </ul>
  </div>
);

Technical.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
  })),
};

Technical.defaultProps = {
  data: [],
};

export default Technical;
