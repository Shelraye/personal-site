import React from 'react';
import PropTypes from 'prop-types';

const Skill = ({ data, last }) => (
  <li className="course-container">
    <h6 className="course-name">{data.title}</h6>
    {!last && <div className="course-dot"><p className="course-name"> &#8226;</p></div>}
  </li>
);

Skill.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
  }).isRequired,
  last: PropTypes.bool,
};

Skill.defaultProps = {
  last: false,
};


export default Skill;
