import React from 'react';
import { Link } from 'react-router-dom';

import Main from '../layouts/Main';

import Education from '../components/Resume/Education';
import Experience from '../components/Resume/Experience';
import References from '../components/Resume/References';
import Skill from '../components/Resume/Technical';

import degrees from '../data/resume/degrees';
import positions from '../data/resume/positions';
import skills from '../data/resume/skills';


const sections = [
  'Skills',
  'Education',
  'Experience',
  'References',
];

const Resume = () => (
  <Main
    title="Resume"
    description="Shelen R. Ells' Resume. Rocket Mortgage, Donor Network of Arizona, St. Joseph's Hospital and Medical Center, Banner Thunderbird Medical Center, Mt. Graham Regional Medical Center."
  >
    <article className="post" id="resume">
      <header>
        <div className="title">
          <h2 data-testid="heading"><Link to="resume">Resume</Link></h2>
          <div className="link-container">
            {sections.map((sec) => (
              <h4 key={sec}>
                <a href={`#${sec.toLowerCase()}`}>{sec}</a>
              </h4>))}
          </div>

        </div>
      </header>
      <Skill data={skills} />
      <Education data={degrees} />
      <Experience data={positions} />
      <References />

    </article>
  </Main>
);

export default Resume;
